# Sante Coffeee

An Android Application that helps Sante Coffee merchants onboard farmers faster and more efficiently.

# Getting started
1. Open Android studio, and choose checkout project from Version Control
2. Choose Git and input the repository url.
3. Clone the project to your preferred folder.
4. The project's modules will then be built incrementally by gradle. 
Make sure you're connected to the internet to download all the project dependencies.
5. Setup a run configuration choosing either a connected android device or an emulator and
run the application.

## More info
The entry point of the project is the app module. To get a better understanding of the navigation flow,
navigate to the navigation module, goto the res folder, choose the navigation folder and click the
navigation_graph.xml.
Next, click the design tab to view the different ui screens.

### Author - Abraham Arishian.
