package com.laboremus.santecoffee;


import androidx.test.espresso.intent.rule.IntentsTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.laboremus.santecoffee.custommatchers.CustomMatchers;
import com.laboremus.santecoffee.views.MainActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasErrorText;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.StringContains.containsString;

@RunWith(AndroidJUnit4.class)
public class OnBoardingFarmerFragmentInstrumentedTest {
    
    @Rule
    public IntentsTestRule<MainActivity> mMainActivityTestRule =
            new IntentsTestRule<>(MainActivity.class);
    @Before
    public void setupFragment() {
        mMainActivityTestRule.getActivity().getSupportFragmentManager().beginTransaction();
    }

    public void navigateToOnBoardingScreen(){
        onView(withId(R.id.btn_google_signin)).perform(click());
    }

    @Test
    public void clickSubmitButton_WithOneOrMoreTextFieldsEmpty_ShowsError() {

        navigateToOnBoardingScreen();

        // scroll to the position of the button targeted
        onView(withId(R.id.btn_continue_on_boarding)).perform(scrollTo());

        // check if submit button is displayed
        onView(withId(R.id.btn_continue_on_boarding)).check(matches(isDisplayed()));

        onView(withId(R.id.btn_continue_on_boarding)).perform(click());

        onView((withId(R.id.ed_surname))).check(matches(hasErrorText("Required")));
    }

    @Test
    public void clickSubmitButton_WithAllTextFieldsFilled_ShowsNoErrors(){

        String surname = "";
        String givenName = "Abraham";
        String sex = "M";
        String dateOfBirth = "11/09/1994";
        String dateOfExpiry = "11/09/2022";
        String NIN = "CMC12364784907";
        String village = "Erisa";
        String parish = "Kyebando";
        String subCounty = "Kawempe";
        String county = "Kawempe";
        String district = "Wakiso";
        String phoneNumber = "07888604752";

        // navigate to on boarding screen
        onView(withId(R.id.btn_google_signin)).perform(click());

        onView(withId(R.id.ed_surname)).check(matches(isDisplayed()));
        onView(withId(R.id.ed_surname)).perform(scrollTo());
        onView(withId(R.id.ed_surname)).perform(typeText(surname));

        onView(withId(R.id.ed_given_name)).perform(scrollTo());
        onView(withId(R.id.ed_given_name)).perform(typeText(givenName));

        onView(withId(R.id.ed_sex)).perform(scrollTo());
        onView(withId(R.id.ed_sex)).perform(typeText(sex));

        onView(withId(R.id.ed_date_of_birth)).perform(scrollTo());
        onView(withId(R.id.ed_date_of_birth)).perform(typeText(dateOfBirth));

        onView(withId(R.id.ed_date_of_expiry)).perform(scrollTo());
        onView(withId(R.id.ed_date_of_expiry)).perform(typeText(dateOfExpiry));

        onView(withId(R.id.ed_nin)).perform(scrollTo());
        onView(withId(R.id.ed_nin)).perform(typeText(NIN));

        onView(withId(R.id.ed_village)).perform(scrollTo());
        onView(withId(R.id.ed_village)).perform(typeText(village));

        onView(withId(R.id.ed_parish)).perform(scrollTo());
        onView(withId(R.id.ed_parish)).perform(typeText(parish));

        onView(withId(R.id.ed_sub_county)).perform(scrollTo());
        onView(withId(R.id.ed_sub_county)).perform(typeText(subCounty));

        onView(withId(R.id.ed_county)).perform(scrollTo());
        onView(withId(R.id.ed_county)).perform(typeText(county));

        onView(withId(R.id.ed_district)).perform(scrollTo());
        onView(withId(R.id.ed_district)).perform(typeText(district));

        onView(withId(R.id.ed_phone)).perform(scrollTo());
        onView(withId(R.id.ed_phone)).perform(typeText(phoneNumber));

        onView(withId(R.id.btn_continue_on_boarding)).perform(scrollTo());
        onView(withId(R.id.btn_continue_on_boarding)).perform(click());

        onView(withId(R.id.ed_surname)).check(matches(withText(containsString(surname))));
        onView(withId(R.id.ed_given_name)).check(matches(withText(containsString(givenName))));
        onView(withId(R.id.ed_sex)).check(matches(withText(containsString(sex))));
        onView(withId(R.id.ed_date_of_expiry)).check(matches(withText(containsString(dateOfExpiry))));
        onView(withId(R.id.ed_date_of_birth)).check(matches(withText(containsString(dateOfBirth))));
        onView(withId(R.id.ed_nin)).check(matches(withText(containsString(NIN))));
        onView(withId(R.id.ed_village)).check(matches(withText(containsString(village))));
        onView(withId(R.id.ed_parish)).check(matches(withText(containsString(parish))));
        onView(withId(R.id.ed_sub_county)).check(matches(withText(containsString(subCounty))));
        onView(withId(R.id.ed_county)).check(matches(withText(containsString(county))));
        onView(withId(R.id.ed_district)).check(matches(withText(containsString(district))));
        onView(withId(R.id.ed_phone)).check(matches(withText(containsString(phoneNumber))));
    }

    @Test
    public void clickSubmitButton_WithWrongSexSpecified_ShowsErrors(){

        String surname = "Asiimirwe";
        String givenName = "Abraham";
        String sex = "w";
        String dateOfBirth = "11/09/1994";
        String dateOfExpiry = "11/09/2022";
        String NIN = "CMC12364784907";
        String village = "Erisa";
        String parish = "Kyebando";
        String subCounty = "Kawempe";
        String county = "Kawempe";
        String district = "Wakiso";
        String phoneNumber = "07888604752";

        // navigate to on boarding screen
        onView(withId(R.id.btn_google_signin)).perform(click());

        onView(withId(R.id.ed_surname)).check(matches(isDisplayed()));
        onView(withId(R.id.ed_surname)).perform(scrollTo());
        onView(withId(R.id.ed_surname)).perform(typeText(surname));

        onView(withId(R.id.ed_given_name)).perform(scrollTo());
        onView(withId(R.id.ed_given_name)).perform(typeText(givenName));

        onView(withId(R.id.ed_sex)).perform(scrollTo());
        onView(withId(R.id.ed_sex)).perform(typeText(sex));

        onView(withId(R.id.ed_date_of_birth)).perform(scrollTo());
        onView(withId(R.id.ed_date_of_birth)).perform(typeText(dateOfBirth));

        onView(withId(R.id.ed_date_of_expiry)).perform(scrollTo());
        onView(withId(R.id.ed_date_of_expiry)).perform(typeText(dateOfExpiry));

        onView(withId(R.id.ed_nin)).perform(scrollTo());
        onView(withId(R.id.ed_nin)).perform(typeText(NIN));

        onView(withId(R.id.ed_village)).perform(scrollTo());
        onView(withId(R.id.ed_village)).perform(typeText(village));

        onView(withId(R.id.ed_parish)).perform(scrollTo());
        onView(withId(R.id.ed_parish)).perform(typeText(parish));

        onView(withId(R.id.ed_sub_county)).perform(scrollTo());
        onView(withId(R.id.ed_sub_county)).perform(typeText(subCounty));

        onView(withId(R.id.ed_county)).perform(scrollTo());
        onView(withId(R.id.ed_county)).perform(typeText(county));

        onView(withId(R.id.ed_district)).perform(scrollTo());
        onView(withId(R.id.ed_district)).perform(typeText(district));

        onView(withId(R.id.ed_phone)).perform(scrollTo());
        onView(withId(R.id.ed_phone)).perform(typeText(phoneNumber));

        onView(withId(R.id.btn_continue_on_boarding)).perform(scrollTo());
        onView(withId(R.id.btn_continue_on_boarding)).perform(click());

        onView(withId(R.id.ed_sex)).perform(scrollTo());
        onView((withId(R.id.ed_sex))).check(matches(hasErrorText("Wrong sex specified, can only be M or F")));

    }

    @Test
    public void clickSubmitButton_WithAllTextFieldsFilled_ShowsRequiredBirthCertificateToast(){
        clickSubmitButton_WithAllTextFieldsFilled_ShowsNoErrors();
        onView(withText(R.string.birth_certificate_required_toast_message)).inRoot(CustomMatchers.INSTANCE.isToast()).check(matches(isDisplayed()));
    }

//    @Test
//    public void clickCameraIcon_LaunchesInAppCamera() {
//        navigateToOnBoardingScreen();
//        // Build the result to return when the activity is launched.
//
//        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
//        Bundle bundle = new Bundle();
//        bundle.putParcelable("IMG_DATA", BitmapFactory.decodeResource(context.getResources(), R.drawable.birth_certificate));
//
//        Intent resultData = new Intent();
//        resultData.putExtras(bundle);
//
//        Instrumentation.ActivityResult result =
//                new Instrumentation.ActivityResult(Activity.RESULT_OK, resultData);
//
//        // Set up result stubbing when an intent sent to "camera" is seen.
//        intending(hasAction(MediaStore.ACTION_IMAGE_CAPTURE)).respondWith(result);
//
//
//        // launch camera app
//        onView(withId(R.id.img_tap_to_capture)).perform(click());
//
//        onView(withId(R.id.img_upload)).check(matches(CustomMatchers.INSTANCE.hasImageSet()));
//
//    }


}
