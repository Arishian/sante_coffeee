package com.laboremus.santecoffee.views

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.laboremus.santecoffee.R
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        
        supportActionBar?.setDisplayHomeAsUpEnabled(false)

        val navController = findNavController(R.id.nav_host_fragment)

        // Set root level destinations
        val topLevelDestinations = HashSet<Int>()
        topLevelDestinations.add(R.id.onBoardingFragment)
        topLevelDestinations.add(R.id.searchFragment)

        // connect drawer to navigation graph
        val appBarConfiguration = AppBarConfiguration(topLevelDestinations, drawer_layout)

        // set up toolbar with navigation ui
        findViewById<Toolbar>(R.id.toolbar)
            .setupWithNavController(
                navController,
                appBarConfiguration
            )
        // set up side menu view with navigation ui
        findViewById<NavigationView>(R.id.nav_view)
            .setupWithNavController(navController)
    }

}
