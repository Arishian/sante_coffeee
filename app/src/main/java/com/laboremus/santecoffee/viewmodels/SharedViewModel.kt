package com.laboremus.santecoffee.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SharedViewModel : ViewModel() {
    val inputNumber = MutableLiveData<Int>()
}