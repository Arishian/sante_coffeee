package com.laboremus.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.laboremus.database.entities.Manager
import com.laboremus.repositories.ManagerRepository

class ManagerViewModel(application: Application) : AndroidViewModel(application) {

    private val mManagerRepository: ManagerRepository = ManagerRepository(application)

    fun getManager(email: String): LiveData<Manager> {
        return mManagerRepository.getManager(email)
    }
    fun createManager(manager: Manager) {
        mManagerRepository.insert(manager)
    }
}