package com.laboremus.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.laboremus.database.entities.Log
import com.laboremus.repositories.LogsRepository


class LogsViewModel(application: Application) : AndroidViewModel(application) {

    private val mLogsRepository: LogsRepository = LogsRepository(application)

    fun logUpdate(log: Log) {
        mLogsRepository.insert(log)
    }
}