package com.laboremus.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.laboremus.database.entities.Farmer
import com.laboremus.repositories.FarmerRepository

class FarmerViewModel(application: Application) : AndroidViewModel(application) {

    private val mFarmerRepository: FarmerRepository = FarmerRepository(application)
    private val farmers: LiveData<List<Farmer>>
    val mFarmer = MutableLiveData<Farmer>()

    init {
        farmers = mFarmerRepository.farmers
    }

    fun passFarmerToNextFragment(farmer: Farmer){
        mFarmer.value = farmer
    }
    fun getFarmers(): LiveData<List<Farmer>> {
        return farmers
    }

    fun createFarmerRecord(farmer: Farmer) {
        mFarmerRepository.insert(farmer)
    }

    fun updatePhoneNumber(farmer: Farmer) {
        mFarmerRepository.updatePhone(farmer)
    }
}