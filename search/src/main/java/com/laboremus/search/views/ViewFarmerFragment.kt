package com.laboremus.search.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.laboremus.search.R
import com.laboremus.viewmodels.FarmerViewModel
import kotlinx.android.synthetic.main.fragment_view_farmer.*
import android.graphics.BitmapFactory
import android.util.Base64
import android.widget.ImageView


class ViewFarmerFragment : Fragment() {

    private lateinit var mSharedViewModel: FarmerViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mSharedViewModel = activity?.run {
            ViewModelProviders.of(this)[FarmerViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_view_farmer, container, false)

        // populate fields
        mSharedViewModel.mFarmer.observe(this, Observer {farmer ->
            if(farmer != null) {
                // set birthcertificate image
                decodeStringToImage(farmer.birthCertificate, img_upload)
                tv_surname_val.text = farmer.surname
                tv_given_name_val.text = farmer.givenName
                tv_nationality_val.text = farmer.nationality
                tv_nin_val.text = farmer.nin
                tv_sex_val.text = farmer.sex
                tv_dob_val.text = farmer.dateOfBirth
                tv_doe_val.text = farmer.dateOfExpiry
                tv_village_val.text = farmer.village
                tv_parish_val.text = farmer.parish
                tv_sub_county_val.text = farmer.subCounty
                tv_county_val.text = farmer.county
                tv_district_val.text = farmer.district
                tv_phone_no_val.text = farmer.phoneNumber
            }
        })
        return rootView
    }

    private fun decodeStringToImage(base64String: String, image: ImageView) {
        //decode base64 string to image

        val imageBytes = Base64.decode(base64String, Base64.DEFAULT)
        val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        image.setImageBitmap(decodedImage)
    }
}