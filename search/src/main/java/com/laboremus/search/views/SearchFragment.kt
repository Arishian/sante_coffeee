package com.laboremus.search.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.laboremus.database.entities.Farmer
import com.laboremus.search.R
import com.laboremus.search.adapters.FarmersAdapter
import com.laboremus.viewmodels.FarmerViewModel
import com.mancj.materialsearchbar.MaterialSearchBar
import kotlinx.android.synthetic.main.fragment_search.view.*


class SearchFragment : Fragment(), FarmersAdapter.FarmersListItemClickListener,
    MaterialSearchBar.OnSearchActionListener {

    private lateinit var mFarmersRecyclerView: RecyclerView
    private lateinit var mFarmersAdapter: RecyclerView.Adapter<*>
    private lateinit var mFarmersViewManager: RecyclerView.LayoutManager

    private lateinit var mFarmersList: ArrayList<Farmer>

    private lateinit var model: FarmerViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = activity?.run {
            ViewModelProviders.of(this)[FarmerViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_search, container, false)

        mFarmersAdapter = FarmersAdapter(this, rootView.context)
        mFarmersViewManager = LinearLayoutManager(rootView.context)
        mFarmersRecyclerView = rootView.findViewById<RecyclerView>(R.id.rv_farmers_records).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = mFarmersViewManager

            // specify a viewAdapter
            adapter = mFarmersAdapter

        }

        loadFarmers()

        rootView.searchBar.setOnSearchActionListener(this)
        return rootView
    }

    override fun onButtonClicked(buttonCode: Int) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSearchStateChanged(enabled: Boolean) {
        val s = if (enabled) "enabled" else "disabled"
        if (s == "disabled") {
            loadFarmers()
        }
    }

    override fun onSearchConfirmed(text: CharSequence?) {
        val searchResults = ArrayList<Farmer>()
        for (i in 0 until mFarmersList.size) {
            if (mFarmersList[i].surname.contains(
                    text!!,
                    ignoreCase = true
                ) || mFarmersList[i].givenName.contains(
                    text,
                    ignoreCase = true
                ) || mFarmersList[i].phoneNumber.contains(text, ignoreCase = true)
            ) {
                searchResults.add(mFarmersList[i])
            }
        }
        (mFarmersAdapter as FarmersAdapter).setFarmers(searchResults)
        mFarmersAdapter.notifyDataSetChanged()
    }

    override fun onFarmersListItemClick(farmer: Farmer) {
        // navigate to view farmer screen
        findNavController().navigate(R.id.viewFarmerFragment)
        model.passFarmerToNextFragment(farmer)
    }

    override fun onEdit(farmer: Farmer) {
        // navigate to edit screen
        findNavController().navigate(R.id.editFragment)
        model.passFarmerToNextFragment(farmer)
    }


    private fun loadFarmers() {
        val mFarmersViewModel = ViewModelProviders.of(this).get(FarmerViewModel::class.java)

        mFarmersViewModel.getFarmers().observe(this, Observer { farmers ->

            // Update the cached copy of the farmers in the adapter.
            if (farmers != null) {
                (mFarmersAdapter as FarmersAdapter).setFarmers(farmers)
                mFarmersList = farmers as ArrayList<Farmer>
                mFarmersAdapter.notifyDataSetChanged()
            }

        })

    }
}