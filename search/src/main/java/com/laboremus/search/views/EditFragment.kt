package com.laboremus.search.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.laboremus.database.entities.Farmer
import com.laboremus.database.entities.Log
import com.laboremus.search.R
import com.laboremus.viewmodels.FarmerViewModel
import com.laboremus.viewmodels.LogsViewModel
import kotlinx.android.synthetic.main.fragment_edit.*
import kotlinx.android.synthetic.main.fragment_edit.view.*
import java.sql.Timestamp
import java.util.*


class EditFragment : Fragment() {

    private lateinit var mSharedViewModel: FarmerViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       mSharedViewModel = activity?.run {
            ViewModelProviders.of(this)[FarmerViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_edit, container, false)
        rootView.btn_submit_edit.setOnClickListener {
            performUpdateAndLogUserChanges()
        }
        return rootView
    }

    private fun performUpdateAndLogUserChanges() {
        val logsViewModel = ViewModelProviders.of(this).get(LogsViewModel::class.java)
        var mFarmer: Farmer? = null
        mSharedViewModel.mFarmer.observe(this, Observer { farmer ->
            if (farmer != null) {
                mFarmer = farmer
                mFarmer!!.phoneNumber = ed_edit_phone?.text.toString()
            }
        })

        mSharedViewModel.updatePhoneNumber(mFarmer!!)

        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            val date = Date()
            val timestamp = Timestamp(date.time)
            // log manager who made changes
            val log = Log(mFarmer!!.nin, user.email!!, timestamp.toString(), false)
            logsViewModel.logUpdate(log)

            // go back to previous page
            findNavController().navigate(R.id.searchFragment)
        } else {
            Toast.makeText(context, "User is not signed in", Toast.LENGTH_SHORT).show()
        }
    }


}

