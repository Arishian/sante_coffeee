package com.laboremus.search.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.laboremus.database.entities.Farmer
import com.laboremus.search.R

class FarmersAdapter(private val mFarmersListItemClickListener: FarmersListItemClickListener, private val context: Context) :
    RecyclerView.Adapter<FarmersAdapter.ViewHolder>() {

    private var mFarmersList: List<Farmer>? = null // Cached copy of farmers

    interface FarmersListItemClickListener {
        fun onFarmersListItemClick(farmer: Farmer)
        fun onEdit(farmer: Farmer)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cardFarmer: CardView? = view.findViewById(R.id.cardview_farmer)
        var fullName: TextView? = view.findViewById(R.id.tv_fullname_value)
        var phone: TextView? = view.findViewById(R.id.tv_phone_value)
        var uploadStatus: TextView? = view.findViewById(R.id.tv_upload_status_value)
        var btnEdit: MaterialButton? = view.findViewById(R.id.btn_edit)

        init {
            btnEdit?.setOnClickListener {
                val farmer = mFarmersList?.get(adapterPosition)
                if (farmer != null) {
                    mFarmersListItemClickListener.onEdit(farmer)
                }
            }

            cardFarmer?.setOnClickListener {
                val farmer = mFarmersList?.get(adapterPosition)
                if (farmer != null) {
                    mFarmersListItemClickListener.onFarmersListItemClick(farmer)
                }
            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val layoutFarmerItem = R.layout.recyclerview_farmer_item
        val attachToRootFalse = false

        val farmerViewHolder = LayoutInflater.from(context)
            .inflate(layoutFarmerItem, parent, attachToRootFalse)

        return ViewHolder(farmerViewHolder)
    }

    override fun getItemCount(): Int {
        return mFarmersList?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val fullName =
            "${mFarmersList?.get(position)?.surname} ${mFarmersList?.get(position)?.givenName}"
        var uploadStatus = "Queued"
        holder.fullName?.text = fullName
        holder.phone?.text = mFarmersList?.get(position)?.phoneNumber
        if(!mFarmersList?.get(position)!!.isSynced) {
            holder.uploadStatus?.text = uploadStatus
            holder.uploadStatus?.setTextColor(ContextCompat.getColor(context, R.color.colorQueued))
        } else {
            uploadStatus = "Successful"
            holder.uploadStatus?.text = uploadStatus
            holder.uploadStatus?.setTextColor(ContextCompat.getColor(context, R.color.colorSuccess))

        }
    }

    fun setFarmers(farmers: List<Farmer>) {
        mFarmersList = farmers
    }


}