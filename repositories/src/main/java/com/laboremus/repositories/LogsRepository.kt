package com.laboremus.repositories

import android.app.Application
import android.os.AsyncTask
import com.laboremus.database.AppDatabase
import com.laboremus.database.entities.Log
import com.laboremus.database.queries.LogsDao

class LogsRepository(application: Application) {

    private val mLogsDao: LogsDao

    init {
        val db = AppDatabase.getDatabase(application)
        mLogsDao = db.logsDao()
    }

    fun insert(log: Log) {
        InsertAsyncTask(mLogsDao).execute(log)
    }


    private class InsertAsyncTask internal constructor(private val mAsyncTaskDao: LogsDao) : AsyncTask<Log, Void, Void>() {
        override fun doInBackground(vararg logs: Log): Void? {
            mAsyncTaskDao.insert(logs[0])
            return null
        }

    }

}