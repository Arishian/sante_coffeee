package com.laboremus.repositories

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.laboremus.database.AppDatabase
import com.laboremus.database.entities.Manager
import com.laboremus.database.queries.ManagerDao

class ManagerRepository(application: Application) {

    private val mManagerDao: ManagerDao

    private var mManager: LiveData<Manager>? = null

    init {
        val db = AppDatabase.getDatabase(application)
        mManagerDao = db.managerDao()
    }

    fun getManager(email: String): LiveData<Manager> {
        GetAsyncTask(mManagerDao).execute(email)
        return mManager!!
    }

    fun insert(manager: Manager) {
        InsertAsyncTask(mManagerDao).execute(manager)
    }

    inner class GetAsyncTask internal constructor(private val mAsyncTaskDao: ManagerDao) : AsyncTask<String, Void, LiveData<Manager>>() {
        override fun doInBackground(vararg emails: String): LiveData<Manager>? {
            return mAsyncTaskDao.getManager(emails[0])
        }

        override fun onPostExecute(result: LiveData<Manager>?) {
            super.onPostExecute(result)
            mManager = result!!
        }

    }
    private class InsertAsyncTask internal constructor(private val mAsyncTaskDao: ManagerDao) : AsyncTask<Manager, Void, Void>() {
        override fun doInBackground(vararg managers: Manager): Void? {
            mAsyncTaskDao.insert(managers[0])
            return null
        }

    }

}