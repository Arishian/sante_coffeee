package com.laboremus.repositories

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.laboremus.database.AppDatabase
import com.laboremus.database.entities.Farmer
import com.laboremus.database.queries.FarmerDao

class FarmerRepository(application: Application) {

    private val mFarmerDao: FarmerDao

    val farmers: LiveData<List<Farmer>>

    init {
        val db = AppDatabase.getDatabase(application)
        mFarmerDao = db.farmerDao()
        farmers = mFarmerDao.getFarmers()
    }

    fun insert(farmer: Farmer) {
        InsertAsyncTask(mFarmerDao).execute(farmer)
    }

    fun updatePhone(farmer: Farmer) {
        UpdateAsyncTask(mFarmerDao).execute(farmer)
    }


    private class InsertAsyncTask internal constructor(private val mAsyncTaskDao: FarmerDao) :
        AsyncTask<Farmer, Void, Void>() {
        override fun doInBackground(vararg farmers: Farmer): Void? {
            mAsyncTaskDao.insert(farmers[0])
            return null
        }

    }

    private class UpdateAsyncTask internal constructor(private val mAsyncTaskDao: FarmerDao) :
        AsyncTask<Farmer, Void, Void>() {
        override fun doInBackground(vararg farmers: Farmer): Void? {
            mAsyncTaskDao.updatePhoneNumber(farmers[0].farmerId, farmers[0].phoneNumber)
            return null
        }

    }

}