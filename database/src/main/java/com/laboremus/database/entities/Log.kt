package com.laboremus.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Log(
    @PrimaryKey(autoGenerate = true) val logId: Int,
    @ColumnInfo(name = "farmer_nin") val farmerNin: String,
    @ColumnInfo(name = "email") val managerEmail: String,
    @ColumnInfo(name = "timestamp") val timestamp: String,
    @ColumnInfo(name = "is_synced") val isSynced: Boolean
){
    constructor(
        farmerNin: String,
        managerEmail: String,
        timestamp: String,
        isSynced: Boolean
    ): this(0,farmerNin,managerEmail,timestamp,isSynced)
}