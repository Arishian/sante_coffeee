package com.laboremus.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Farmer(
    @PrimaryKey(autoGenerate = true) val farmerId: Int,
    @ColumnInfo(name = "birth_certificate") val birthCertificate: String,
    @ColumnInfo(name = "surname") val surname: String,
    @ColumnInfo(name = "given_name") val givenName: String,
    @ColumnInfo(name = "nationality") val nationality: String,
    @ColumnInfo(name = "sex") val sex: String,
    @ColumnInfo(name = "date_of_birth") val dateOfBirth: String,
    @ColumnInfo(name = "date_of_expiry") val dateOfExpiry: String,
    @ColumnInfo(name = "nin") val nin: String,
    @ColumnInfo(name = "village") val village: String,
    @ColumnInfo(name = "parish") val parish: String,
    @ColumnInfo(name = "sub_county") val subCounty: String,
    @ColumnInfo(name = "county") val county: String,
    @ColumnInfo(name = "district") val district: String,
    @ColumnInfo(name = "phone_number") var phoneNumber: String,
    @ColumnInfo(name = "is_synced") val isSynced: Boolean
) {
    constructor(
        birthCertificate: String,
        surname: String,
        givenName: String,
        nationality: String,
        sex: String,
        dateOfBirth: String,
        dateOfExpiry: String,
        nin: String,
        village: String,
        parish: String,
        subCounty: String,
        county: String,
        district: String,
        phoneNumber: String,
        isSynced: Boolean
    ) : this(
        0,
        birthCertificate,
        surname,
        givenName,
        nationality,
        sex,
        dateOfBirth,
        dateOfExpiry,
        nin,
        village,
        parish,
        subCounty,
        county,
        district,
        phoneNumber,
        isSynced
    )
}
