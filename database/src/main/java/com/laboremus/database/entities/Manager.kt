package com.laboremus.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Manager(
    @PrimaryKey(autoGenerate = true) val managerId: Int,
    @ColumnInfo(name = "email") val managerEmail: String,
    @ColumnInfo(name = "full_name") val fullName: String,
    @ColumnInfo(name = "is_synced") val isSynced: Boolean
){
    constructor(
        managerEmail: String,
        fullName: String,
        isSynced: Boolean
    ): this(0,managerEmail,fullName,isSynced)
}