package com.laboremus.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.laboremus.database.entities.Farmer
import com.laboremus.database.entities.Log
import com.laboremus.database.entities.Manager
import com.laboremus.database.queries.FarmerDao
import com.laboremus.database.queries.LogsDao
import com.laboremus.database.queries.ManagerDao


@Database(entities = [Farmer::class, Log::class, Manager::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun farmerDao(): FarmerDao
    abstract fun logsDao(): LogsDao
    abstract fun managerDao(): ManagerDao

    companion object {
        fun getDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java, "farmer_database"
            ).build()
        }
    }
}