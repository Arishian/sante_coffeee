package com.laboremus.database.queries

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.laboremus.database.entities.Manager

@Dao
interface ManagerDao {

    @Query("SELECT * FROM manager WHERE email = :email ")
    fun getManager(email: String): LiveData<Manager>

    @Insert
    fun insert(manager: Manager)
}