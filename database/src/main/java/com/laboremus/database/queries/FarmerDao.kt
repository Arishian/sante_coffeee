package com.laboremus.database.queries

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.laboremus.database.entities.Farmer

@Dao
interface FarmerDao {
    
    @Query("SELECT * FROM farmer")
    fun getFarmers(): LiveData<List<Farmer>>

    @Insert
    fun insert(farmer: Farmer)

    @Query("UPDATE farmer SET phone_number = :phone WHERE farmerId = :fid")
    fun updatePhoneNumber(fid: Int, phone: String): Int
}