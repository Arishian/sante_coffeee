package com.laboremus.database.queries

import androidx.room.Dao
import androidx.room.Insert
import com.laboremus.database.entities.Log

@Dao
interface LogsDao {
    @Insert
    fun insert(log: Log)
}