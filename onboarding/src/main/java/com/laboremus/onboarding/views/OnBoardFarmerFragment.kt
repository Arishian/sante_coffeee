package com.laboremus.onboarding.views

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.laboremus.database.entities.Farmer
import com.laboremus.onboarding.R
import com.laboremus.onboarding.views.utils.HelperMethods.clearImageCaptureAfterFormSubmission
import com.laboremus.onboarding.views.utils.HelperMethods.clearInputFieldsAfterFormSubmission
import com.laboremus.onboarding.views.utils.HelperMethods.encodeBitmapToBase64String
import com.laboremus.onboarding.views.utils.HelperMethods.hasNoEmptyFields
import com.laboremus.onboarding.views.utils.HelperMethods.hasTakenBirthCertficatePhoto
import com.laboremus.onboarding.views.utils.HelperMethods.hideViews
import com.laboremus.onboarding.views.utils.HelperMethods.isSexValid
import com.laboremus.onboarding.views.utils.HelperMethods.restrictInputTextToUpperCase
import com.laboremus.onboarding.views.utils.HelperMethods.showViews
import com.laboremus.viewmodels.FarmerViewModel
import kotlinx.android.synthetic.main.fragment_on_board_farmer.*
import kotlinx.android.synthetic.main.fragment_on_board_farmer.view.*


class OnBoardFarmerFragment : Fragment() {

    val RequestPermissionCode = 1

    private lateinit var birthCertificateBitmap: String
    private lateinit var surname: String
    private lateinit var givenName: String
    private lateinit var nationality: String
    private lateinit var sex: String
    private lateinit var dateOfBirth: String
    private lateinit var dateOfExpiry: String
    private lateinit var nin: String
    private lateinit var village: String
    private lateinit var parish: String
    private lateinit var subCounty: String
    private lateinit var county: String
    private lateinit var district: String
    private lateinit var phone: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // This callback will only be called when this fragment is at least Started.
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // close application at this destination
                (activity as AppCompatActivity).finish()
            }

        }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_on_board_farmer, container, false)
        // show activity toolbar after signin
        (activity as AppCompatActivity).supportActionBar?.show()

        restrictInputTextToUpperCase(ed_sex)

        // initialize values
        rootView.btn_continue_on_boarding.setOnClickListener {
            if (hasNoEmptyFields(
                    ed_surname,
                    ed_given_name,
                    ed_nationality,
                    ed_sex,
                    ed_date_of_birth,
                    ed_date_of_expiry,
                    ed_nin,
                    ed_village,
                    ed_parish,
                    ed_sub_county,
                    ed_county,
                    ed_district,
                    ed_phone
                )
                && isSexValid(ed_sex)
            ) {
                if (hasTakenBirthCertficatePhoto(img_upload)) {
                    surname = ed_surname?.text.toString()
                    givenName = ed_given_name?.text.toString()
                    nationality = ed_nationality?.text.toString()
                    sex = ed_sex?.text.toString()
                    dateOfBirth = ed_date_of_birth?.text.toString()
                    dateOfExpiry = ed_date_of_expiry?.text.toString()
                    nin = ed_nin?.text.toString()
                    village = ed_village?.text.toString()
                    parish = ed_parish?.text.toString()
                    subCounty = ed_sub_county?.text.toString()
                    county = ed_county?.text.toString()
                    district = ed_district?.text.toString()
                    phone = ed_phone?.text.toString()
                } else {
                    Toast.makeText(
                        rootView.context,
                        getString(R.string.birth_certificate_required_toast_message),
                        Toast.LENGTH_SHORT
                    ).show()
                }


                // queue farmer record for upload
//                addFarmerToLocalDB()
            }

        }
        // for android M and above, request camera runtime permission
        requestRuntimePermission()

        rootView.img_tap_to_capture.setOnClickListener {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, 7)
        }
        return rootView
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == 7 && resultCode == RESULT_OK) {
            val bitmap = data!!.extras!!.get("data") as Bitmap

            hideViews(img_tap_to_capture, tv_tap_to_capture)
            showViews(btn_retake_snap)

            // display image in view provided
            img_upload.setImageBitmap(bitmap)

            // encode image as string for upload
            birthCertificateBitmap = encodeBitmapToBase64String(bitmap)

            // retake photo in case of unclear picture
            btn_retake_snap.setOnClickListener {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(intent, 7)
            }
        }
    }

    private fun requestRuntimePermission() {
        if (ContextCompat.checkSelfPermission(
                activity as AppCompatActivity,
                Manifest.permission.READ_CONTACTS
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            // provide an explanation only if the user has already denied that permission request.
            // otherwise continue
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    activity as AppCompatActivity,
                    Manifest.permission.CAMERA
                )
            ) {
                Toast.makeText(
                    activity,
                    "The camera permission is required for you to take a photo of your birth certificate",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                ActivityCompat.requestPermissions(
                    activity as AppCompatActivity,
                    arrayOf(Manifest.permission.CAMERA),
                    RequestPermissionCode
                )

            }
        }
    }


    // TODO: Refactor addFarmerToLocalDB method to comply with defensive coding techniques
    // this will act as a cache of farmers
    private fun addFarmerToLocalDB() {
        val mFarmer =
            Farmer(
                birthCertificateBitmap,
                surname,
                givenName,
                nationality,
                sex,
                dateOfBirth,
                dateOfExpiry,
                nin,
                village,
                parish,
                subCounty,
                county,
                district,
                phone,
                false
            )

        var hasItemBeenAdded = false
        val mFarmersViewModel = ViewModelProviders.of(this).get(FarmerViewModel::class.java)

        mFarmersViewModel.getFarmers().observe(this, Observer { farmers ->
            if (farmers != null) {
                // avoid adding duplicate entries
                for (i in 0 until farmers.size) {
                    if (farmers[i].nin == nin) {
                        hasItemBeenAdded = true
                        break
                    }
                }
            }
        })

        if (hasItemBeenAdded) {
            Toast.makeText(activity, "Duplicate Farmer entry", Toast.LENGTH_LONG).show()
        } else {
            mFarmersViewModel.createFarmerRecord(mFarmer)

            clearInputFieldsAfterFormSubmission(
                ed_surname,
                ed_given_name,
                ed_sex,
                ed_date_of_birth,
                ed_date_of_expiry,
                ed_nin,
                ed_village,
                ed_parish,
                ed_sub_county,
                ed_county,
                ed_district,
                ed_phone
            )

            clearImageCaptureAfterFormSubmission(img_upload)
            showViews(img_tap_to_capture, tv_tap_to_capture)
            hideViews(btn_retake_snap)

            Toast.makeText(
                activity,
                "Queued entry for upload, you can continue onboarding new farmers.",
                Toast.LENGTH_LONG
            ).show()
        }

    }


}