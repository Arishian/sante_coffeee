package com.laboremus.onboarding.views.utils

import android.graphics.Bitmap
import android.media.Image
import android.text.InputFilter
import android.util.Base64
import android.view.View
import android.widget.ImageView
import com.google.android.material.textfield.TextInputEditText
import java.io.ByteArrayOutputStream
import java.security.cert.Certificate
import java.util.regex.Pattern

object HelperMethods {

    fun hasNoEmptyFields(vararg textFields: TextInputEditText?): Boolean {

        var hasNoEmptyField = true
        for (textField in textFields) {
            if (textField?.text.toString().isEmpty()) {
                textField?.error = "Required"
                hasNoEmptyField = false
            }
        }

        return hasNoEmptyField
    }

    fun isEmailValid(email: String): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

    fun restrictInputTextToUpperCase(vararg textFields: TextInputEditText?){
        for (textField in textFields) {
            textField?.filters = arrayOf<InputFilter>(InputFilter.AllCaps())
        }
    }

    fun isSexValid(sex: TextInputEditText): Boolean {
        val mSex = sex.text.toString()
        if (mSex != "M" || mSex != "F") {
            sex.error = "Wrong sex specified, can only be M or F"
            return false
        }
        return true
    }

    fun hasTakenBirthCertficatePhoto(birthCertificatePhoto: ImageView): Boolean {
        if (birthCertificatePhoto.drawable != null) {
            return true
        }
        return false
    }

    fun encodeBitmapToBase64String(bitmap: Bitmap): String {
        val mByteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, mByteArrayOutputStream)
        val imageBytes = mByteArrayOutputStream.toByteArray()
        val imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT)

        return imageString
    }

    fun clearInputFieldsAfterFormSubmission(vararg textFields: TextInputEditText?) {
        for (textField in textFields) {
            textField?.setText("")
        }
    }

    fun clearImageCaptureAfterFormSubmission(imageView: ImageView?) {
        imageView?.setImageResource(0)
    }

    fun hideViews(vararg views: View) {
        for (view in views) {
            view.visibility = View.GONE
        }
    }

    fun showViews(vararg views: View) {
        for (view in views) {
            view.visibility = View.VISIBLE
        }
    }
}